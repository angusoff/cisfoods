<?php
// HTTP
define('HTTP_SERVER',	'http://cis.anguskh.com/admin/');
define('HTTP_CATALOG',	'http://cis.anguskh.com/');

// HTTPS
define('HTTPS_SERVER',	'http://cis.anguskh.com/admin/');
define('HTTPS_CATALOG',	'http://cis.anguskh.com/');

// DIR
define('DIR_APPLICATION',	'/www/wwwroot/cisfoods/admin/');
define('DIR_SYSTEM',		'/www/wwwroot/cisfoods/system/');
define('DIR_IMAGE',			'/www/wwwroot/cisfoods/image/');
define('DIR_STORAGE',		'/www/wwwroot/storage_cis/');
define('DIR_CATALOG',		'/www/wwwroot/cisfoods/catalog/');
define('DIR_LANGUAGE',		DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE',		DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG',		DIR_SYSTEM . 'config/');
define('DIR_CACHE',			DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD',		DIR_STORAGE . 'download/');
define('DIR_LOGS',			DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION',	DIR_STORAGE . 'modification/');
define('DIR_SESSION',		DIR_STORAGE . 'session/');
define('DIR_UPLOAD',		DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER',		'mysqli');
define('DB_HOSTNAME',	'localhost');
define('DB_USERNAME',	'cisfoods');
define('DB_PASSWORD',	'R7km3kyzMJDzJJkA');
define('DB_DATABASE',	'cisfoods');
define('DB_PORT',		'3306');
define('DB_PREFIX',		'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');

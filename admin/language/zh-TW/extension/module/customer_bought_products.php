<?php
// Heading
$_['heading_title']    		= '會員已購買商品';

$_['column_image']          = '圖片';
$_['column_product_name']   = '商品名稱';
$_['column_quantity']       = '數量';

$_['text_products_bought']  = '已購買商品';

$_['tab-product']  			= '已購買商品';

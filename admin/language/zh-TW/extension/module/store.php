<?php
// Heading
$_['heading_title']    = '商店';

// Text
$_['text_extension']   = '擴充模組';
$_['text_success']     = '成功：您已修改商店模組！';
$_['text_edit']        = '編輯商店模組';

// Entry
$_['entry_admin']      = '限管理員';
$_['entry_status']     = '狀態';

//Help
$_['help_admin']       = 'If yes, then multi store list will be visible only when admin user login!';

// Error
$_['error_permission'] = '警告：您沒有權限更改商店模組！';
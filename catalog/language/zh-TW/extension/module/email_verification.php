<?php

$_['error_approved'] 			= '警告: 會員帳號啟用通知信件已發送至您的 email，請點擊信件中的帳號啟用連結來啟用您的帳號，若您未收到會員帳號啟用通知信，請點擊 <a href="%s">再次發送帳號啟用通知信</a>。';
$_['error_verification'] 		= '錯誤: Email 帳號驗證失敗. 請聯絡網站管理員.';
$_['heading_title'] 			= '會員帳號驗證';
$_['heading_verification'] 		= '驗證您的會員帳號';
$_['success_verified'] 			= '您的 Email 帳號已通過驗證! 您可以從以下登入網站';
$_['text_email_verification'] 	= '請點擊以下連結以啟用您的會員帳號:';
$_['text_resent_verified'] 		= '會員帳號啟用通知信件已發送至您的 email (%s). 請點擊信件中的帳號啟用連結來啟用您的帳號!';
$_['text_services'] 			= 'Upon logging in, you will be able to generate tracking codes, track commission payments and edit your account information.';
$_['text_subject'] 				= '%s - 會員帳號啟用通知';
$_['text_thanks'] 				= '感謝您,';
$_['text_update_email'] 		= '會員帳號啟用通知信件已發送至您的 email，請點擊信件中的帳號啟用連結來啟用您的帳號!';
$_['text_verification'] 		= '<p><b>會員帳號啟用通知信件已發送至您的 email : %s</b></p><p>請點擊信件中的帳號啟用連結來啟用您的帳號.</p><p>若您未收到會員帳號啟用通知信，請點擊 <a href="%s">再次發送帳號啟用通知信</a>。</p>';
$_['text_verified'] 			= 'Verification';
$_['text_welcome'] 				= '歡迎您註冊 %s 會員!';
